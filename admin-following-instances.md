# Following other instances

Following servers and being followed as a server ensure visibility of your videos on other instances, and visibility of their videos on your instance. Both are important concepts of PeerTube as they allow instances and their users to interact.

!> **What is a "follow":** a follow is [a kind of activity](https://www.w3.org/TR/activitypub/#follow-activity-inbox) in the ActivityPub linguo. It allows to subscribe to a server's user activity in the PeerTube realm.

## Following an instance

Following an instance will display videos of that instance on your pages (i.e. "Trending", "Recently Added", etc.) and your users will be able to interact with them.

You can discover other public instances on https://instances.joinpeertube.org/instances.

## Managing follows

You can add an instance to follow and remove instances you follow in `Administration > Manage Follows > Follow`, and add hostnames of the instances you want to follow there.

![Adding servers to follow](/assets/admin-add-follow.png)

## Being followed by an instance

Being followed will display videos of your instance on your followers' pages, and their users will be able to interact with your videos.

You cannot yet refuse a follow as it is automatically accepted, but you can block the instance _a posteriori_.

## Unofficial auto follow scripts

!> If you want to follow every other PeerTube instance, beware that you blindly trust every other admin for enforcing sane moderation policies 

 * [Peetube AutoDiscover](https://framagit.org/Jorropo/peertube-autodiscover) by Jorropo, that uses following and followers list of nodes you already follow to discover new nodes. 
   * Can discover nodes unlisted in the official list (if an admin forgot to add their node in the official list this isn't a problem) as it recursively asks nodes you already know
   * Doesn't require a central node exchange point (node list) 
   * More secure than shell script (shell script use file as variable for storage of login token) 
 * [script from Skid](https://github.com/Chocobozzz/PeerTube/issues/406#issuecomment-397452809) and [python3 version](https://framagit.org/Jorropo/peertube-autodiscover/blob/master/updateFromInstancesList.py) 
   * Faster 
   * Protects privacy (maybe if a node isn't listed on [instances.joinpeertube.org](https://instances.joinpeertube.org/instances) there is a reason)

You can do a cron to automate the update:
 
```
*/10 * * * * /my_script.py >/dev/null 2>&1
```
